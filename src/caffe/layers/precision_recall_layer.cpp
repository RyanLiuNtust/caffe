#include <algorithm>
#include <functional>
#include <utility>
#include <vector>

#include "caffe/layer.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/vision_layers.hpp"

namespace caffe {

template <typename Dtype>
void PrecisionRecallLayer<Dtype>::LayerSetUp(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
}

template <typename Dtype>
void PrecisionRecallLayer<Dtype>::Reshape(
  const vector<Blob<Dtype>*>& bottom, const vector<Blob<Dtype>*>& top) {
  label_axis_ =
      bottom[0]->CanonicalAxisIndex(this->layer_param_.precision_recall_param().axis());
  outer_num_ = bottom[0]->count(0, label_axis_);
  inner_num_ = bottom[0]->count(label_axis_ + 1);
  CHECK_EQ(outer_num_ * inner_num_, bottom[1]->count())
      << "Number of labels must match number of predictions; "
      << "e.g., if label axis == 1 and prediction shape is (N, C, H, W), "
      << "label count (number of labels) must be N*H*W, "
      << "with integer values in {0, 1, ..., C-1}.";
  top[0]->Reshape(1, 3, 1, 1);
}

template <typename Dtype>
void PrecisionRecallLayer<Dtype>::Forward_cpu(const vector<Blob<Dtype>*>& bottom,
    const vector<Blob<Dtype>*>& top) {
  Dtype tp = 0;
  Dtype fp = 0;
  Dtype tn = 0;
  Dtype fn = 0;
  Dtype precision = 0;
  Dtype recall = 0;
  Dtype fp_rate = 0;
  const Dtype* bottom_data = bottom[0]->cpu_data();
  const Dtype* bottom_label = bottom[1]->cpu_data();
  const int dim = bottom[0]->count() / outer_num_;
  const int num_labels = bottom[0]->shape(label_axis_);
  for (int i = 0; i < outer_num_; ++i) {
    for (int j = 0; j < inner_num_; ++j) {
      const int label_value =
          static_cast<int>(bottom_label[i * inner_num_ + j]);
      DCHECK_GE(label_value, 0);
      DCHECK_LT(label_value, num_labels);
      //// Top-k accuracy
      std::vector<std::pair<Dtype, int> > bottom_data_vector;
      for (int k = 0; k < num_labels; ++k) {
        bottom_data_vector.push_back(std::make_pair(
            bottom_data[i * dim + k * inner_num_ + j], k));
      }
      std::partial_sort(
          bottom_data_vector.begin(), bottom_data_vector.begin(),
          bottom_data_vector.end(), std::greater<std::pair<Dtype, int> >());
      if (label_value == 0) {
        //label 0 :cat
        if(bottom_data_vector[0].second == 0) {
          ++tp;
        }
        else {
          ++fn;
        }
      }
      else {
        if(bottom_data_vector[0].second == 1) {
          ++tn;
        }
        else {
          ++fp;
        }
      }

    }
  }

  precision = (tp > 0) ? (tp / (tp + fp)) : 0;
  recall = (tp > 0) ? (tp / (tp + fn)) : 0;
  fp_rate = (fp > 0) ? (fp / (fp + tn)) : 0;
  DLOG(INFO) << "Precison: " << precision;
  DLOG(INFO) << "Recall: " << recall;
  DLOG(INFO) << "False_positive_rate: " << fp_rate;
  top[0]->mutable_cpu_data()[0] = precision; 
  top[0]->mutable_cpu_data()[1] = recall;
  top[0]->mutable_cpu_data()[2] = fp_rate;
  // Accuracy layer should not be used as a loss function.
}

INSTANTIATE_CLASS(PrecisionRecallLayer);
REGISTER_LAYER_CLASS(PrecisionRecall);

}  // namespace caffe
