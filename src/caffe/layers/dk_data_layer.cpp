#include <opencv2/core/core.hpp>

#include <stdint.h>

#include <string>
#include <vector>
#include <fstream>

#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/benchmark.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"

namespace caffe {

template <typename Dtype>
DKDataLayer<Dtype>::~DKDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}


template <typename Dtype>
void DKDataLayer<Dtype>::ReadImgTagFile(string tag_filename, string soft_target_filename) {
  LOG(INFO) << "Opening soft target file " << soft_target_filename;
  std::ifstream softfile(soft_target_filename.c_str());
  map<string, vector<float> > data_vecs;
  string img_name;
  string line;
  float vec;
  while (getline(softfile, line)) {
    std::istringstream iss(line);
    iss >> img_name;
    vector<float> vecs;
    while(iss >> vec) {
      vecs.push_back(vec);
    }
    data_vecs[img_name] = vecs;
  }

  LOG(INFO) << "Opening tag file " << tag_filename;
  std::ifstream tagfile(tag_filename.c_str());
  // read first line from tag file to get total number of label
  // to initialize the data_ first dimension
  string num_labels;
  getline(tagfile, num_labels);
  // check num_labels whether it is a number. std::isdigit()
  int total_num_labels = atoi(num_labels.c_str());
  int label;
  LOG(INFO) << "READ FILE\n";
  data_.resize(total_num_labels);
  while (tagfile >> img_name >> label) {
    data_[label].push_back(make_pair(img_name, data_vecs[img_name]));
  }
  max_data_size_label_ = -1;
  int max_element_position = 0;
  for(vector<vector<pair<std::string, vector<float> > > >::iterator it = data_.begin();
      it != data_.end();
      it++) {
    if(it->size() > data_[max_element_position].size()) {
      max_element_position = std::distance(data_.begin(), it);
    }
  }
  max_data_size_label_ = max_element_position;
  
  LOG(INFO) << "max_data_size_label_ = " << max_data_size_label_;
  CHECK_NE(max_data_size_label_, -1);
}

template <typename Dtype>
void DKDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  DKDataParameter dk_data_param = this->layer_param_.dk_data_param();
  const bool is_color = dk_data_param.is_color();
  const string root_folder = dk_data_param.root_folder();
  // image will use original size when new_height or new_width use default value 
  const int min_img_size = dk_data_param.min_img_size();
  const int max_img_size = dk_data_param.max_img_size();

  CHECK_GT(min_img_size, 0);
  CHECK_GT(max_img_size, min_img_size);
  
  ReadImgTagFile(dk_data_param.source(0), dk_data_param.source(1));
  int total_num_labels = data_.size();
  data_id_.resize(total_num_labels);
  float total_ratio = 0;
  // data_ratios_ will set as 1 / total_num_labels when data_ratio is not specified
  if (dk_data_param.data_ratio_size() == 0) {
    data_ratios_.resize(total_num_labels);
    float ratio = 1./total_num_labels;
    std::fill(data_ratios_.begin(), data_ratios_.end(), ratio);
    for (int i = 0; i < data_ratios_.size(); i++)
      total_ratio += ratio;
  }
  else {
    for (int i = 0; i < dk_data_param.data_ratio_size(); ++i) {
      data_ratios_.push_back(dk_data_param.data_ratio(i));
      total_ratio += dk_data_param.data_ratio(i);
    }
  }
  CHECK_EQ(total_ratio, 1);
  //check tag is whether available
  others_tag_ = dk_data_param.others_tag();

  if (dk_data_param.shuffle()) {
    LOG(INFO) << "Shuffling data";
    const unsigned int prefetch_rng_seed = caffe_rng_rand();
    prefetch_rng_.reset(new Caffe::RNG(prefetch_rng_seed));
    for(int label = 0; label < data_.size(); ++label) {
      ShuffleImages(label);
    }
  }
  // Read an image, and use it to initialize the top blob.
  cv::Mat cv_img = ReadImageToCVMatWithAspectRatio(
      root_folder + data_[0][0].first,
      min_img_size, max_img_size, is_color);
  CHECK(cv_img.data);
  // Use data_transformer to infer the expected blob shape from a cv_image.
  vector<int> top_shape = this->data_transformer_->InferBlobShape(cv_img);
  this->transformed_data_.Reshape(top_shape);
  // Reshape prefetch_data and top[0] according to the batch_size.
  const int batch_size = dk_data_param.batch_size();
  top_shape[0] = batch_size;
  this->prefetch_data_.Reshape(top_shape);
  top[0]->ReshapeLike(this->prefetch_data_);

  LOG(INFO) << "output data size: " << top[0]->num() << ","
      << top[0]->channels() << "," << top[0]->height() << ","
      << top[0]->width();
  // label
  vector<int> label_shape(1, batch_size);
  top[1]->Reshape(label_shape);
  this->prefetch_label_.Reshape(label_shape);

  LOG(INFO) << "set up soft";
  Datum datum;
  int soft_target_vec_size = data_[0][0].second.size();
  LOG(INFO) << "soft target size:" << soft_target_vec_size;
  top[2]->Reshape(batch_size, soft_target_vec_size, 1, 1);
  this->prefetch_soft_target_.Reshape(batch_size, soft_target_vec_size, 1, 1);
  LOG(INFO) << "soft target data size: " << top[2]->num() << ","
      << top[2]->channels() << "," << top[2]->height() << ","
      << top[2]->width();
}

template <typename Dtype>
void DKDataLayer<Dtype>::ShuffleImages(int label) {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(data_[label].begin(), data_[label].end(), prefetch_rng);
}

template <typename Dtype>
void DKDataLayer<Dtype>::DatumToBlob(const Datum& datum, Blob<Dtype>* transformed_blob) {
  const int datum_channels = datum.channels();
  const int datum_height = datum.height();
  const int datum_width = datum.width();
  float datum_element;
  int index;

  Dtype* transformed_data = transformed_blob->mutable_cpu_data();

  for (int c = 0; c < datum_channels; ++c) {
    for (int h = 0; h < datum_height; ++h) {
      for (int w = 0; w < datum_width; ++w) {
        index = (c * datum_height + h) * datum_width + w;
        datum_element = static_cast<float> (datum.float_data(index));
        transformed_data[index] = datum_element;
      }
    }
  }
}

// This function is used to create a thread that prefetches the data.
template <typename Dtype>
void DKDataLayer<Dtype>::InternalThreadEntry() {
  CPUTimer batch_timer;
  batch_timer.Start();
  double read_time = 0;
  double trans_time = 0;
  CPUTimer timer;
  CHECK(this->prefetch_data_.count());
  CHECK(this->transformed_data_.count());
  CHECK(this->prefetch_soft_target_.count());
  DKDataParameter dk_data_param = this->layer_param_.dk_data_param();
  const int batch_size = dk_data_param.batch_size();
  const bool is_color = dk_data_param.is_color();
  string root_folder = dk_data_param.root_folder();
  const int min_img_size = dk_data_param.min_img_size();
  const int max_img_size = dk_data_param.max_img_size();

  CHECK_GT(min_img_size, 0);
  cv::Mat cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[0][data_id_[0]].first,
      min_img_size, max_img_size, is_color);
  // Use data_transformer to infer the expected blob shape from a cv_img.
  vector<int> top_shape = this->data_transformer_->InferBlobShape(cv_img);
  this->transformed_data_.Reshape(top_shape);
  // Reshape prefetch_data according to the batch_size.
  top_shape[0] = batch_size;
  this->prefetch_data_.Reshape(top_shape);
  int soft_target_vec_size = data_[0][0].second.size();
  this->prefetch_soft_target_.Reshape(batch_size, soft_target_vec_size, 1, 1);

  Dtype* prefetch_data = this->prefetch_data_.mutable_cpu_data();
  Dtype* prefetch_label = this->prefetch_label_.mutable_cpu_data();
  Dtype* prefetch_soft_target = this->prefetch_soft_target_.mutable_cpu_data();
  Blob<Dtype> soft_target_blob(1, soft_target_vec_size, 1, 1);

  // datum scales
  int item_id = 0;
  for(int label = 0; label < data_.size(); ++label) {
    timer.Start();
    int specific_data_batch = round(data_ratios_[label] * batch_size);
    for(int i = 0; i < specific_data_batch; ++i) {
      int id = data_id_[label];
      cv::Mat cv_img;
      if (others_tag_ == label) {
        cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[label][id].first,
          min_img_size, is_color);
      }
      else { 
        cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[label][id].first,
          min_img_size, max_img_size, is_color);
      }
      CHECK(cv_img.data) << "Could not load " << data_[label][id].first;
      read_time += timer.MicroSeconds();
      timer.Start();
      int offset = this->prefetch_data_.offset(item_id);
      this->transformed_data_.set_cpu_data(prefetch_data + offset);
      this->data_transformer_->Transform(cv_img, &(this->transformed_data_));
      trans_time += timer.MicroSeconds();
      prefetch_label[item_id] = label;

      //for(int i = 0; i < data_[label][id].second.size(); i++) {
      //  LOG(INFO)  << data_[label][id].second[i];
      //}
      int soft_target_offset = this->prefetch_soft_target_.offset(item_id);
      soft_target_blob.set_cpu_data(prefetch_soft_target + soft_target_offset);
      DatumToBlob(FloatVecToDatum(data_[label][id].second), &(soft_target_blob));

      // go to the next iter
      item_id++;
      data_id_[label]++;
      if (data_id_[label] >= data_[label].size()) {
       // We have reached the end. Restart from the first.
        DLOG(INFO) << "Restarting data prefetching from start.";
        data_id_[label] = 0;
        if (dk_data_param.shuffle()) {
          ShuffleImages(label);
        }
        //refresh tagfile when data which is biggest data in dataset already be done
        if (label == max_data_size_label_) {
          data_.clear();
          ReadImgTagFile(dk_data_param.source(0), dk_data_param.source(1));
        }
      }
    }
  }
  CHECK_EQ(item_id, batch_size);
  batch_timer.Stop();
  DLOG(INFO) << "Prefetch batch: " << batch_timer.MilliSeconds() << " ms.";
  DLOG(INFO) << "     Read time: " << read_time / 1000 << " ms.";
  DLOG(INFO) << "Transform time: " << trans_time / 1000 << " ms.";

}

INSTANTIATE_CLASS(DKDataLayer);
REGISTER_LAYER_CLASS(DKData);

}  // namespace caffe
