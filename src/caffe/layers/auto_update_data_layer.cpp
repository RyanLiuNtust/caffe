#include <opencv2/core/core.hpp>

#include <stdint.h>

#include <string>
#include <vector>

#include "caffe/common.hpp"
#include "caffe/data_layers.hpp"
#include "caffe/layer.hpp"
#include "caffe/proto/caffe.pb.h"
#include "caffe/util/benchmark.hpp"
#include "caffe/util/io.hpp"
#include "caffe/util/math_functions.hpp"
#include "caffe/util/rng.hpp"

namespace caffe {

template <typename Dtype>
AutoUpdateDataLayer<Dtype>::~AutoUpdateDataLayer<Dtype>() {
  this->JoinPrefetchThread();
}


template <typename Dtype>
void AutoUpdateDataLayer<Dtype>::ReadImgTagFile(string filename) {
  LOG(INFO) << "Opening tag file " << filename;
  std::ifstream infile(filename.c_str());
  // read first line from tag file to get total number of label
  // to initialize the data_ first dimension
  string num_labels;
  getline(infile, num_labels);
  int total_num_labels = atoi(num_labels.c_str());
  string img_name;
  int label;

  LOG(INFO) << "READ FILE\n";
  data_.resize(total_num_labels);
  while (infile >> img_name >> label) {
    data_[label].push_back(img_name);
  }
  more_data_label_ = -1;
  int last_tag = data_.size() - 1;
  for(int label = 0; label < last_tag; ++label) {
      LOG(INFO) << "label " << label << ": " <<data_[label].size();
      if (data_[label].size() > data_[label + 1].size()) {
        more_data_label_ = label;
        continue;
      }
      more_data_label_ = label + 1;
  }
  LOG(INFO) << "label " << last_tag << ": " << data_[last_tag].size();
  LOG(INFO) << "more_data_label_ = " << more_data_label_;
  CHECK_NE(more_data_label_, -1);
}

template <typename Dtype>
void AutoUpdateDataLayer<Dtype>::DataLayerSetUp(const vector<Blob<Dtype>*>& bottom,
      const vector<Blob<Dtype>*>& top) {
  AutoUpdateDataParameter auto_update_data_param = this->layer_param_.auto_update_data_param();
  const bool is_color = auto_update_data_param.is_color();
  const string root_folder = auto_update_data_param.root_folder();
  const string& source = auto_update_data_param.source();
  // image will use original size when new_height or new_width use default value 
  const int min_img_size = auto_update_data_param.min_img_size();
  const int max_img_size = auto_update_data_param.max_img_size();

  CHECK_GT(min_img_size, 0);
  CHECK_GT(max_img_size, min_img_size);
  
  ReadImgTagFile(source);
  int total_num_labels = data_.size();
  data_id_.resize(total_num_labels);
  float total_ratio = 0;
  // data_ratios_ will set as 1 / total_num_labels when data_ratio is not specified
  if (auto_update_data_param.data_ratio_size() == 0) {
    data_ratios_.resize(total_num_labels);
    float ratio = 1./total_num_labels;
    std::fill(data_ratios_.begin(), data_ratios_.end(), ratio);
    for (int i = 0; i < data_ratios_.size(); i++)
      total_ratio += ratio;
  }
  else {
    for (int i = 0; i < auto_update_data_param.data_ratio_size(); ++i) {
      data_ratios_.push_back(auto_update_data_param.data_ratio(i));
      total_ratio += auto_update_data_param.data_ratio(i);
    }
  }
  CHECK_EQ(total_ratio, 1);
  //check tag is whether available
  others_tag_ = auto_update_data_param.others_tag();

  if (auto_update_data_param.shuffle()) {
    LOG(INFO) << "Shuffling data";
    const unsigned int prefetch_rng_seed = caffe_rng_rand();
    prefetch_rng_.reset(new Caffe::RNG(prefetch_rng_seed));
    for(int label = 0; label < data_.size(); ++label) {
      ShuffleImages(label);
    }
  }
  // Read an image, and use it to initialize the top blob.
  cv::Mat cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[0][data_id_[0]],
      min_img_size, max_img_size, is_color);
  CHECK(cv_img.data);
  // Use data_transformer to infer the expected blob shape from a cv_image.
  vector<int> top_shape = this->data_transformer_->InferBlobShape(cv_img);
  this->transformed_data_.Reshape(top_shape);
  // Reshape prefetch_data and top[0] according to the batch_size.
  const int batch_size = auto_update_data_param.batch_size();
  top_shape[0] = batch_size;
  this->prefetch_data_.Reshape(top_shape);
  top[0]->ReshapeLike(this->prefetch_data_);

  LOG(INFO) << "output data size: " << top[0]->num() << ","
      << top[0]->channels() << "," << top[0]->height() << ","
      << top[0]->width();
  // label
  vector<int> label_shape(1, batch_size);
  top[1]->Reshape(label_shape);
  this->prefetch_label_.Reshape(label_shape);
}

template <typename Dtype>
void AutoUpdateDataLayer<Dtype>::ShuffleImages(int label) {
  caffe::rng_t* prefetch_rng =
      static_cast<caffe::rng_t*>(prefetch_rng_->generator());
  shuffle(data_[label].begin(), data_[label].end(), prefetch_rng);
}

// This function is used to create a thread that prefetches the data.
template <typename Dtype>
void AutoUpdateDataLayer<Dtype>::InternalThreadEntry() {
  CPUTimer batch_timer;
  batch_timer.Start();
  double read_time = 0;
  double trans_time = 0;
  CPUTimer timer;
  CHECK(this->prefetch_data_.count());
  CHECK(this->transformed_data_.count());
  AutoUpdateDataParameter auto_update_data_param = this->layer_param_.auto_update_data_param();
  const int batch_size = auto_update_data_param.batch_size();
  const bool is_color = auto_update_data_param.is_color();
  string root_folder = auto_update_data_param.root_folder();
  const int min_img_size = auto_update_data_param.min_img_size();
  const int max_img_size = auto_update_data_param.max_img_size();

  CHECK_GT(min_img_size, 0);
  cv::Mat cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[0][data_id_[0]],
      min_img_size, max_img_size, is_color);
  // Use data_transformer to infer the expected blob shape from a cv_img.
  vector<int> top_shape = this->data_transformer_->InferBlobShape(cv_img);
  this->transformed_data_.Reshape(top_shape);
  // Reshape prefetch_data according to the batch_size.
  top_shape[0] = batch_size;
  this->prefetch_data_.Reshape(top_shape);

  Dtype* prefetch_data = this->prefetch_data_.mutable_cpu_data();
  Dtype* prefetch_label = this->prefetch_label_.mutable_cpu_data();

  // datum scales
  int item_id = 0;
  for(int label = 0; label < data_.size(); ++label) {
    timer.Start();
    int specific_data_batch = round(data_ratios_[label] * batch_size);
    for(int i = 0; i < specific_data_batch; ++i) {
      int id = data_id_[label];
      cv::Mat cv_img;
      if (others_tag_ == label) {
        cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[label][id],
          min_img_size, is_color);
      }
      else { 
        cv_img = ReadImageToCVMatWithAspectRatio(root_folder + data_[label][id],
          min_img_size, max_img_size, is_color);
      }
      CHECK(cv_img.data) << "Could not load " << data_[label][id];
      read_time += timer.MicroSeconds();
      timer.Start();
      int offset = this->prefetch_data_.offset(item_id);
      this->transformed_data_.set_cpu_data(prefetch_data + offset);
      this->data_transformer_->Transform(cv_img, &(this->transformed_data_));
      trans_time += timer.MicroSeconds();
      prefetch_label[item_id] = label;
      // go to the next iter
      item_id++;
      data_id_[label]++;
      if (data_id_[label] >= data_[label].size()) {
       // We have reached the end. Restart from the first.
        DLOG(INFO) << "Restarting data prefetching from start.";
        data_id_[label] = 0;
        if (auto_update_data_param.shuffle()) {
          ShuffleImages(label);
        }
        //refresh tagfile when data which is biggest data in dataset already be done
        if (label == more_data_label_) {
            data_.clear();
            ReadImgTagFile(auto_update_data_param.source());
        }
      }
    }
  }
  CHECK_EQ(item_id, batch_size);
  batch_timer.Stop();
  DLOG(INFO) << "Prefetch batch: " << batch_timer.MilliSeconds() << " ms.";
  DLOG(INFO) << "     Read time: " << read_time / 1000 << " ms.";
  DLOG(INFO) << "Transform time: " << trans_time / 1000 << " ms.";

}

INSTANTIATE_CLASS(AutoUpdateDataLayer);
REGISTER_LAYER_CLASS(AutoUpdateData);

}  // namespace caffe
